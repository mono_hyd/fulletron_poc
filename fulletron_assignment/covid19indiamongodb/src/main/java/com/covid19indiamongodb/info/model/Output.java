package com.covid19indiamongodb.info.model;

import java.util.Arrays;

public class Output {
	private Travel_History[] travel_history;

	
	public Travel_History[] getTravel_history() {
		return travel_history;
	}


	public void setTravel_history(Travel_History[] travel_history) {
		this.travel_history = travel_history;
	}


	@Override
	public String toString() {
		return "Output [travel_history=" + Arrays.toString(travel_history) + "]";
	}


	

	

	
	

}
