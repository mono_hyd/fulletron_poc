package com.covid19indiamongodb.info.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.covid19indiamongodb.info.model.Output;
import com.covid19indiamongodb.info.model.StandardApiResponseEntity;
import com.covid19indiamongodb.info.model.Travel_History;
import com.covid19indiamongodb.info.repository.TravelInfoMongoRepository;
import com.covid19indiamongodb.info.service.TravelInfoService;

@Service
public class TravelInfoServiceImpl implements TravelInfoService {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	TravelInfoMongoRepository travelInfoRepository;

	@Value("${travelInfoURL}")
	String travelInfoURL;

	@Override
	public StandardApiResponseEntity saveTravelInfoToMongoDB() throws Exception {
		Output travelinfo = restTemplate.getForObject(travelInfoURL, Output.class);
		for (Travel_History data : travelinfo.getTravel_history()) {
			travelInfoRepository.save(data);
		}
		return new StandardApiResponseEntity(travelinfo);
	}

}
