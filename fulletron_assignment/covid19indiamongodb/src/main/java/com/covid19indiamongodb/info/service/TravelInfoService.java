package com.covid19indiamongodb.info.service;

import com.covid19indiamongodb.info.model.StandardApiResponseEntity;

public interface TravelInfoService {
	
	 public StandardApiResponseEntity saveTravelInfoToMongoDB() throws Exception;
	 

}
