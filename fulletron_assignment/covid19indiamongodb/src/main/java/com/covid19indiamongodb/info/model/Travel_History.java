package com.covid19indiamongodb.info.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Travel_History")
public class Travel_History {
	@Id
	private Integer _cn6ca;
	private String accuracylocation;
	private String address;
	private String datasource;
	private String latlong;
	private String modeoftravel;
	private String pid;
	private String placename;
	private String timefrom;
	private String timeto;
	private String type;
	private String _d2mkx;

	public String get_d2mkx() {
		return _d2mkx;
	}

	public void set_d2mkx(String _d2mkx) {
		this._d2mkx = _d2mkx;
	}

	public Integer get_cn6ca() {
		return _cn6ca;
	}

	public void set_cn6ca(Integer _cn6ca) {
		this._cn6ca = _cn6ca;
	}

	public String getAccuracylocation() {
		return accuracylocation;
	}

	public void setAccuracylocation(String accuracylocation) {
		this.accuracylocation = accuracylocation;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDatasource() {
		return datasource;
	}

	public void setDatasource(String datasource) {
		this.datasource = datasource;
	}

	public String getLatlong() {
		return latlong;
	}

	public void setLatlong(String latlong) {
		this.latlong = latlong;
	}

	public String getModeoftravel() {
		return modeoftravel;
	}

	public void setModeoftravel(String modeoftravel) {
		this.modeoftravel = modeoftravel;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getPlacename() {
		return placename;
	}

	public void setPlacename(String placename) {
		this.placename = placename;
	}

	public String getTimefrom() {
		return timefrom;
	}

	public void setTimefrom(String timefrom) {
		this.timefrom = timefrom;
	}

	public String getTimeto() {
		return timeto;
	}

	public void setTimeto(String timeto) {
		this.timeto = timeto;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "TravelInfo [_cn6ca=" + _cn6ca + ", accuracylocation=" + accuracylocation + ", address=" + address
				+ ", datasource=" + datasource + ", latlong=" + latlong + ", modeoftravel=" + modeoftravel + ", pid="
				+ pid + ", placename=" + placename + ", timefrom=" + timefrom + ", timeto=" + timeto + ", type=" + type
				+ "]";
	}

}
