package com.covid19india.info.model;

public abstract class ResponseEntity {
	private Status status;

	private Object data;

	public ResponseEntity() {
		this.setSuccessStatus();
	}

	public ResponseEntity(Status status, Object data) {
		super();
		this.setSuccessStatus();
		this.status = status;

		this.data = data;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public void setSuccessStatus() {
		Status status = new Status(200, "Success");
		this.setStatus(status);
	}

	public void setSuccessStatus(String message) {
		Status status = new Status(401, message);
		this.setStatus(status);
	}

	public void setErrorStatus() {
		Status status = new Status(401, "Error");
		this.setStatus(status);
	}

	public void setErrorStatus(String error) {
		Status status = new Status(401, error);
		this.setStatus(status);
	}

	public void setErrorStatus(Exception ex) {
		Status status = new Status(401, ex.getMessage());
		this.setStatus(status);
	}

}
