package com.covid19india.info.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.covid19india.info.model.Output;
import com.covid19india.info.model.StandardApiResponseEntity;
import com.covid19india.info.model.Travel_History;
import com.covid19india.info.repository.TravelInfoMySqlRepository;
import com.covid19india.info.service.TravelInfoService;

@Service
public class TravelInfoServiceImpl implements TravelInfoService {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	TravelInfoMySqlRepository travelInfoRepository;

	@Value("${travelInfoURL}")
	String travelInfoURL;

	@Override
	public StandardApiResponseEntity getTravelInfo() throws Exception {
		travelInfoRepository.findAll();
		return new StandardApiResponseEntity(travelInfoRepository.findAll());
	}

	@Override
	public StandardApiResponseEntity getTravelInfoByDate(String timefrom, String timeto) throws Exception {

		return new StandardApiResponseEntity(travelInfoRepository.findAllByDateNamedParamsNative(timefrom, timeto));
	}

	@Override
	public StandardApiResponseEntity saveTravelInfoToMySqlDB() throws Exception {
		Output travelinfo = restTemplate.getForObject(travelInfoURL, Output.class);
		for (Travel_History data : travelinfo.getTravel_history()) {
			travelInfoRepository.save(data);
		}
		return new StandardApiResponseEntity(travelinfo);
	}

}
