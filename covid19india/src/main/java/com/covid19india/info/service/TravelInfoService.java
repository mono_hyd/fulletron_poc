package com.covid19india.info.service;

import com.covid19india.info.model.StandardApiResponseEntity;

public interface TravelInfoService {
 public StandardApiResponseEntity getTravelInfo() throws Exception;
 public StandardApiResponseEntity getTravelInfoByDate(String timefrom,String timeto) throws Exception;
 public StandardApiResponseEntity saveTravelInfoToMySqlDB() throws Exception;
	
 
}
